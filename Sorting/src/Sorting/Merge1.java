package Sorting;

public class Merge1 
{
//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	public Merge1()
	{
		System.out.println("Merge Sort 1:");
		Sorting.Start(S);
		MergeSort(n, S);		
		Sorting.End(S);
//		System.out.print("Merge Sort 1 ");
		Sorting.CalculateRunningTime();
	}
	
	private static void MergeSort(int n, int S[])
	{		
		if(n > 1)
		{
			int h = (int) Math.floor(n/2);
			int m = n-h;
			
			int U[] = new int[h];
			int V[] = new int[m];
			
			for(int i = 0; i < h; i++)
			{
				U[i] = S[i];
			}
			
			for(int i = 0; i < m; i++)
			{
				V[i] = S[h+i];
			}
			
			MergeSort(h, U);
			MergeSort(m, V);
			Merge(h, m, U, V, S);
			
		}
		
	}

	private static void Merge(int h, int m, int u[], int v[], int s[])
	{
		int i = 0;
		int j = 0;
		int k = 0;
		
		while(i < h && j < m)
		{
			if(u[i] <= v[j])
			{
				s[k] = u[i];
				i++;
			}
			else
			{
				s[k] = v[j];
				j++;
			}
			
			k++;
		}
		
		if(i > h)
		{
			while(j < m)
			{
				s[k] = v[j];
				j++;
			}
		}
		else
		{
			while(i < h)
			{
				s[k] = u [i];
				i++;
			}
			
		}
	}
	
	
}
