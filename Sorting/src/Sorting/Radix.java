package Sorting;
 
public class Radix 
{
//	static int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	static int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	 public Radix() 
	{		
		System.out.println("Heap Sort:");
		Sorting.Start(S);
		Radix radix = new Radix(S);
		Sorting.End(S);
		System.out.print("Heap Sort ");
		Sorting.CalculateRunningTime();
	}
	    
    public Radix( int s[])
    {
        int i; 
        int m = s[0];
        int exp = 1;
        int n = s.length;
        int[] b = new int[n];
        
        for (i = 1; i < n; i++)
            if (s[i] > m)
                m = s[i];
        while (m / exp > 0)
        {
            int[] bucket = new int[n];
 
            for (i = 0; i < n; i++)
            {
                bucket[(s[i] / exp) % n]++;
            }
            for (i = 1; i < n; i++)
            {
                bucket[i] += bucket[i - 1];
            }
            for (i = n - 1; i >= 0; i--)
            {
                b[--bucket[(s[i] / exp) % n]] = s[i];
            }
            for (i = 0; i < n; i++)
            {
                s[i] = b[i];
            }
            exp *= n;        
        }
    }    
   
}