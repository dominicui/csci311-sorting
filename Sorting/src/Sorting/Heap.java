package Sorting;
 
public class Heap
{
//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length-1;
	
	int S[] = Sorting.RandomChain;
	int n = S.length-1;
	
    int left;
    int right;
    int largest;
 
    public Heap() 
	{		
		System.out.println("Heap Sort:");
		Sorting.Start(S);
		Heap heap = new Heap(S);
		Sorting.End(S);
		System.out.print("Heap Sort ");
		Sorting.CalculateRunningTime();
	}
    
    public Heap(int s[])
    {
        S = s;
        buildheap(S);
 
        for (int i = n; i > 0; i--)
        {
            exchange(0, i);
            n = n - 1;
            maxheap(S, 0);
        }
    }
    
    public void buildheap(int s[])
    {
        for (int i = n / 2; i >= 0; i--)
        {
            maxheap(S, i);
        }
    }
 
    public void maxheap(int s[], int i)
    {
        left = 2 * i;
        right = 2 * i + 1;
        
        if (left <= n && S[left] > S[i])
        {
            largest = left;
        } 
        else
        {
            largest = i;
        }
 
        if (right <= n && S[right] > S[largest])
        {
            largest = right;
        }
        
        if (largest != i)
        {
            exchange(i, largest);
            maxheap(S, largest);
        }
    }
 
    public void exchange(int i, int j)
    {
        int t = S[i];
        S[i] = S[j];
        S[j] = t;
    }   

}