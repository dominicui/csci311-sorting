package Sorting;

public class Quick2 
{
//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	public Quick2() 
	{
		System.out.println("Quick Sort 2:");
		Sorting.Start(S);
		QuickSort(0, n);
		Sorting.End(S);
//		System.out.print("Quick Sort 2 ");
		Sorting.CalculateRunningTime();
	}

	private void QuickSort(int low, int high) 
	{
		int pivotpoint = (int) Math.floor((low+high)/2);
		
		if(high > low)
		{
			Partition(low , high, pivotpoint);
			QuickSort(low, pivotpoint-1);
			QuickSort(pivotpoint+1, high);
		}
	}

	private void Partition(int low, int high, int pivotpoint) 
	{
		int i = low;
		int j = high;
		int pivotitem = S[low];
		int temp;
		
		do 
		{
			i++;
		}
		while(i < high && S[i] <= pivotitem);
		
		do
		{
			j--;
		}
		while(S[j] > pivotitem);
		
		while(i < j)
		{
			temp = S[i];
			S[i] = S[j];
			S[j] = temp;
			
			do 
			{
				i++;
			}
			while(S[i] <= pivotitem);
			
			do
			{
				j--;
			}
			while(S[j] > pivotitem);
		}
		
		pivotpoint = j;
		temp = S[low];
		S[low] = S[pivotpoint];
		S[pivotpoint] = temp;
	}

}
