package Sorting;

public class Selection {

//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	
	public Selection() 
	{		
		System.out.println("Selection Sort:");
		Sorting.Start(S);
		Selection sel = new Selection(n, S);
		Sorting.End(S);
		System.out.print("Selection Sort ");
		Sorting.CalculateRunningTime();
	}
	

	public Selection(int n, int s[]) {
		int i;
		int j;
		int smallest;
		for(i = 0; i < n; i++) {
			smallest = i;
			for(j = i + 1; j < n; j++) {
				if(s[j] < s[smallest]) {
					smallest = j;
				}	
			}
			int temp;	
			temp = s[i];
			s[i] = s[smallest];
			s[smallest] = temp;
			
		}
	}


}
