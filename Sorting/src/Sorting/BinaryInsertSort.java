package Sorting;

public class BinaryInsertSort {

//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	public BinaryInsertSort()
	{
		System.out.println("Binary Insert Sort:");
		Sorting.Start(S);
		insertionSort(S, n);		
		Sorting.End(S);
		System.out.print("Binary Insert Sort ");
		Sorting.CalculateRunningTime();
	}
	
	public int binarySearch(int a[], int item, int low, int high)
	{
	    if (high <= low)
	        return (item > a[low])? (low + 1): low;
	 
	    int mid = (low + high)/2;
	 
	    if(item == a[mid])
	        return mid+1;
	 
	    if(item > a[mid])
	        return binarySearch(a, item, mid+1, high);
	    return binarySearch(a, item, low, mid-1);
	}
	 
	public void insertionSort(int a[], int n)
	{
	    int i, loc, j, k, selected;
	 
	    for (i = 1; i < n; ++i)
	    {
	        j = i - 1;
	        selected = a[i];
	 
	        loc = binarySearch(a, selected, 0, j);
	 
	        while (j >= loc)
	        {
	            a[j+1] = a[j];
	            j--;
	        }
	        a[j+1] = selected;
	    }
	}
}
