package Sorting;

public class Insert {

//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	public Insert() 
	{
		System.out.println("Insert Sort:");
		Sorting.Start(S);
		Insert in = new Insert(n, S);
		Sorting.End(S);
		System.out.print("Insert Sort ");
		Sorting.CalculateRunningTime();
	}

	public Insert(int n, int s[]) {
		int i = 0;
		int j = 0;
		int x;
		for(i = 1; i < n; i++) {
			x = s[i];
			j = i - 1;
			while(j > 0 && s[j]> x) {
				s[j + 1] = s[j];
				j--;
			}
			s[j+1] = x;
		}
	}

}
