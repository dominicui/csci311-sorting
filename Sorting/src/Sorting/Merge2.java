package Sorting;

public class Merge2 
{
//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	int U[];
	
	public Merge2() 
	{
		System.out.println("Merge Sort 2:");
		Sorting.Start(S);
		MergeSort(0, n);
		Sorting.End(U);
//		System.out.print("Merge Sort 2 ");
		Sorting.CalculateRunningTime();
	}

	private void MergeSort(int low, int high) 
	{
		int mid;
		
		if(low < high)
		{
			mid = (int) Math.floor((low + high)/2);
			MergeSort(low, mid);
			MergeSort(mid+1, high);
			Merge(low, mid ,high);
		}
	}

	private void Merge(int low, int mid, int high) 
	{
		int i = low;
		int j = mid+1;
		int k = low;
		
		U = new int[high];
		
		while(i < mid && j < high)
		{
			if(S[i] <= S[j])
			{
				U[k] = S[i];
				i++;
			}
			else
			{
				U[k] = S[j];
				j++;
			}
			
			k++;
		}
		
		if(i > mid)
		{
			while(j < high)
			{
				U[k] = S[j];
				j++;
				k++;
			}
		}
		else
		{
			while(i < mid)
			{
				U[k] = S[i];
				i++;
				k++;
			}	
		}
	}

}
