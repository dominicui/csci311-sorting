package Sorting;
 
class Merge4
{
//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	public Merge4() 
	{		
		LinkedList list = new LinkedList();
		for (int i = 0; i < n; i++)
		{
			list.append(S[i]);
		}
		
		System.out.println("Merge Sort 4:");
		Sorting.Start(S);
		list.MergeSort(list.extractFirst());
		
		Selection sel = new Selection(n, S);
		Sorting.End(S);
		System.out.print("Merge Sort 4 ");
		Sorting.CalculateRunningTime();
	}
}

class Node 
{
    public int item;
    public Node next;
 
    public Node(int val) 
    {
        item = val;
    }
 
    public Node() 
    {}
 
    public void displayNode() 
    {
        System.out.print(item);
    }
}
 
class LinkedList 
{
    private Node first;
 
    public LinkedList() 
    {
        first = null;
    }
 
    public boolean isEmpty() 
    {
        return (first == null);
    }
 
    public void append(int val)
    {
        Node newNode = new Node(val);
        newNode.next = first;
        first = newNode;
    }
 
    public Node extractFirst() 
    {
        return first;
    }
 
   public Node MergeSort(Node headOriginal) 
	{
		if (headOriginal == null || headOriginal.next == null)
			return headOriginal;
		Node l1 = headOriginal;
		Node l2 = headOriginal.next;
		while ((l2 != null) && (l2.next != null)) {
			headOriginal = headOriginal.next;
			l2 = (l2.next).next;
		}
		l2 = headOriginal.next;
		headOriginal.next = null;
		return merge(MergeSort(l1), MergeSort(l2));
	}

	public Node merge(Node l1, Node l2) 
	{
		Node temp = new Node();
		Node head = temp;
		Node c = head;
		while ((l1 != null) && (l2 != null)) {
			if (l1.item <= l2.item) {
				c.next = l1;
				c = l1;
				l1 = l1.next;
			} else {
				c.next = l2;
				c = l2;
				l2 = l2.next;
			}
		}
		c.next = (l1 == null) ? l2 : l1;
		return head.next;
	}
}
 
