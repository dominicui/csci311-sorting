package Sorting;

public class Exchange 
{
//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	public Exchange()
	{
		System.out.println("Exchange Sort:");
		Sorting.Start(S);
		ExchangeSort(n, S);
		Sorting.End(S);
		System.out.print("Exchange Sort ");
		Sorting.CalculateRunningTime();
	}
	
	private static void ExchangeSort(int n, int S[])
	{	
		int temp;
		
		for(int i = 0; i <= n-1; i++)
		{
			for(int j = i+1; j <= n-1; j++)
			{
				if(S[j] <= S[i])
				{
					temp = S[j];
					S[j] = S[i];
					S[i] = temp;
				}
			}
		}
		
	}

}
