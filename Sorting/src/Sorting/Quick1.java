package Sorting;

public class Quick1
{
//	int S[] = {2,4,5,7,3,6,1,8,9,10,};
//	int n = S.length;
	
	int S[] = Sorting.RandomChain;
	int n = S.length;
	
	public Quick1() 
	{
		System.out.println("Quick Sort 1:");
		Sorting.Start(S);
		QuickSort(0, n);
		Sorting.End(S);
		System.out.print("Quick Sort 1 ");
		Sorting.CalculateRunningTime();
	}

	private void QuickSort(int low, int high) 
	{
		int pivotpoint = (int) Math.floor((low+high)/2);
		
		if(high > low)
		{
			Partition(low , high, pivotpoint);
			QuickSort(low, pivotpoint-1);
			QuickSort(pivotpoint+1, high);
		}
	}

	private void Partition(int low, int high, int pivotpoint) 
	{
		int i;
		int j = low;
		int pivotitem = S[low];
		int temp;
		
		for(i = low+1; i < high; i ++)
		{
			if(S[i] <= pivotitem) 
			{
				j++;
				temp = S[i];
				S[i] = S[j];
				S[j] = temp;
			}
		}
		
		pivotitem = j;
		temp = S[low];
		S[low] = S[pivotitem];
		S[pivotitem] = temp;
	}

}
