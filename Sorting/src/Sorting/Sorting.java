package Sorting;

import java.util.Random;

public class Sorting 
{
	static int TimesToRun;
	static long start;
	static long end;
	static long RunningTime;
	static long SumTime;
	static long AverageTime;
	static int[] RandomChain;
	static int[] RunningSize = {500};
	
	public static void main(String[] args) 
	{
		TimesToRun = 10;
		
		for(int i = 0; i < RunningSize.length; i++)
		{
			for(int j = 0; j < TimesToRun; j++)
			{
				RandomArray(RunningSize[i]);
				System.out.println(j);
//				new Exchange();
				new Merge1();
//				new Merge2();
//				new Quick1();
//				new Quick2();
//				new Insert();
//				new Selection();
//				new BinaryInsertSort();
//				new Heap();
//				new Radix();
//				new Merge4();
				
				SumTime += RunningTime;
			}
			
			AverageTime = SumTime/TimesToRun;
			System.out.println("Average Time: " + AverageTime + "ns");
			SumTime = 0;
			AverageTime = 0;
		}
		
	}

	public static void Print(int s[])
	{
		for(int i = 0; i < s.length; i++)
		{
			System.out.print(s[i]);
			
		}
		
		System.out.print("\n");
	}
	
	public static void Start(int s[])
	{
		start = System.nanoTime();
		
//		System.out.println("unordered array:");
//		Print(s);
	}
	
	public static void End(int s[])
	{
//		System.out.println("ordered array:");
//		Sorting.Print(s);
		
		end = System.nanoTime();
	}
	
	public static void CalculateRunningTime()
	{
		RunningTime = end - start;
		System.out.println("Running time: " + RunningTime + "ns");
	}
	
	public static int[] RandomArray(int size)
	{
		RandomChain = new int[size];
		for(int i = 0; i < size-1; i++)
		{
			Random num = new Random();
			int data = num.nextInt(size);
			RandomChain[i] = data;
		}
		
		return RandomChain;
	}
	
}
